# bahn-connection-cli

<img src="./assets/logo.png">

Command line interface to retrieve departure and arrival times between a departure and an arrival train station of the Deutsche Bahn.

## Installation

```bash
    npm install -g https://gitlab.com/rosbart/bahn-connection-cli.git#1.0.0
```

## Options

```bash
  -V, --version            output the version number
  -f, --from <from>        Name of the departure train station
  -t, --to <to>            Name of the destination train station
  -a, --at [HH:mm]         Departure time at departure train station (HH:mm) (default: "13:48")
  -d, --date [dd.MM.YYYY]  Departure date at departure train station (dd.MM.YYYY) (default: "12.12.2018")
  -h, --help               output usage information
```

## Example

```bash
bahn-connection-cli -f "München Hbf" -t "Hamburg Altona" -a 10:00 -d 01.01.2019
```

This will list bahn connections from station `München Hbf` to station `Hamburg Altona` on 01.01.2019 at 10:00 o'clock.

## Maintainer

* [Phibart](https://gitlab.com/phibart)
* [Josch Rossa](https://gitlab.com/josros)