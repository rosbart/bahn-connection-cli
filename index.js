#!/usr/bin/env node
require('colors');

const figlet = require('figlet');
const cli = require('commander');
const Table = require('cli-table');
const logSymbols = require('log-symbols');
const { retrieve } = require('./connection-retriever');
const package = require('./package.json');

const log = console.log;
const error = console.error;

const now = new Date();

cli
    .version(package.version)
    .option('-f, --from <from>', 'Name of the departure train station')
    .option('-t, --to <to>', 'Name of the destination train station')
    .option('-a, --at [HH:mm]', 'Departure time at departure train station (HH:mm)', new RegExp('^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$'), `${_numberToString(now.getHours())}:${_numberToString(now.getMinutes())}`)
    .option('-d, --date [dd.MM.YYYY]', 'Departure date at departure train station (dd.MM.YYYY)', new RegExp('(0[1-9]|[12][0-9]|3[01]).(0[1-9]|1[012]).((19|20)\\d\\d)'), `${_numberToString(now.getUTCDate())}.${_numberToString(now.getUTCMonth()+1)}.${_numberToString(now.getFullYear())}`)
    .parse(process.argv);

if(!cli.from){
    error('No departure train station specified!');
    process.exit(1);
}

if(!cli.to){
    error('No destination train station specified!');
    process.exit(1);
}

function _numberToString(number){
    return number < 10 ? `0${number}` : number;
}

function _createConnectionsTable(connections) {
    let table = new Table({
        head: [
            _header(cli.from),
            _header(cli.to),
            _header('Type(s)'),
            _header('Duration')
        ],
        colWidths: [50, 50, 15,15]
    });

    connections.forEach((connection) => {
        table.push([
            _formatTime(connection.departureTime, connection.estimatedDepartureTime),
            _formatTime(connection.arrivalTime, connection.estimatedArrivalTime),
            connection.type,
            `${connection.duration} h`
        ]);
    });

    return table.toString();
}

function _header(text){
    return text.bold.cyan
}

function _formatTime(should, is){
    return `${should} (${should === is ? `${logSymbols.success} ${is.green}` : `${logSymbols.error} ${is.red}`})`;
}

figlet(package.name, async (_, data) => {
    log(`${data.rainbow}\n`);

    try{
      const connections = await retrieve(cli.from, cli.to, cli.date, cli.at);
      const table = _createConnectionsTable(connections);
      log(table);
    } catch(e){
      error(e);
    }

    log(`\nVisit us at: ${package.repository.url.underline}\n`)
});
