const { JSDOM } = require("jsdom");
const { XMLHttpRequest } = require("xmlhttprequest");
const { connections, generate } = require('@rosbart/bahn-connection-retriever/dist/url-generator');
const { toConnections } = require('@rosbart/bahn-connection-retriever/dist/connection-converter');

module.exports = {
    retrieve
}

async function retrieve(start, destination, dateAsString, timeAsString) {
    const date = _toDate(dateAsString);
    const time = _toTime(timeAsString);

    const searchDate = new Date(Date.UTC(date.year, date.month, date.day, time.hours, time.minutes, 0));

    const url = generate(
        connections()
        .from(start)
        .to(destination)
        .at(searchDate)
        .origin('')
    );

    const document = await request(url);

    return toConnections(document, searchDate);
}

function _toDate(dateAsString) {
    const date = dateAsString.split('.');
    return {
        year: date[2],
        month: date[1] - 1,
        day: date[0]
    }
}

function _toTime(timeAsString) {
    const time = timeAsString.split(':');
    return {
        hours: parseInt(time[0]) + (new Date().getTimezoneOffset() / 60),
        minutes: time[1]
    }
}

function request(url) {
    return new Promise((resolve, reject) => {
        try {
            const http = new XMLHttpRequest();
            http.onreadystatechange = () => {
                if (http.readyState == 4 && http.status == 200) {
                    resolve(new JSDOM(http.responseText).window.document);
                } else if(http.readyState == 4) {
                    reject(http.responseText);
                }
            }
            http.open('GET', url, true);
            http.send();
        } catch(e) {
            reject(e);
        }
    });
}